#!/usr/bin/env python

import requests
import json
import os
import datetime

currentTime = datetime.datetime.now().strftime("%Y%m%d%H%M")
currentDate = datetime.datetime.now().strftime("%Y%m%d")
currentDir = os.getcwd()
path = os.path.join(currentDir, currentDate)
try:
    os.mkdir(path)
except OSError as error:
    print(error)
try:
    os.chdir(path)
except:
    print("chdir() error")

with open("nine-days.json", "w") as file:
    x = requests.get("https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=fnd&lang=en")
    data = x.text
    file.write(data)

with open("next-day.json", "w+") as file:
    x = requests.get("https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=flw&lang=en")
    data = x.text
    file.write(data)

with open("aviat_c.json", "w+") as file:
    x = requests.get("https://www.hko.gov.hk/dps/sc/aviat/100nm_html_c.json")
    data = x.text
    file.write(data)

yesterday = datetime.date.today() - datetime.timedelta(days = 1)
with open("ryes.json", "w+") as file:
    x = requests.get("https://data.weather.gov.hk/weatherAPI/opendata/opendata.php?dataType=RYES&date="+str(yesterday.strftime("%Y%m%d"))+"&lang=en")
    print("https://data.weather.gov.hk/weatherAPI/opendata/opendata.php?dataType=RYES&date="+str(yesterday.strftime("%Y%m%d"))+"&lang=en")
    data = x.text
    file.write(data)
