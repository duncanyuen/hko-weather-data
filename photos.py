#!/usr/bin/env python

import urllib.request
import os
import datetime
import random

nt_loc = ["LFS", "WLP", "ELC", "KFB", "TPK", "TM2", "TLC", "SK2", "SKG", "CWB", "CWA", "KS2"]
kln_loc = ["KLT", "HK2", "HKO", "IC2", "IC1"]
hki_loc = ["CP1", "VPB", "VPA", "GSI", "SWH"]
lni_loc = ["SLW", "DNL", "PE2", "CCH", "CCE", "LAM", "WL2", "WGL"]
locs = [nt_loc, kln_loc, hki_loc, lni_loc]

currentDate = datetime.datetime.now().strftime("%Y%m%d")
currentTime = datetime.datetime.now().strftime("%Y%m%d%H%M")
currentDir = os.getcwd()
path = os.path.join(currentDir, currentDate)
try:
    os.mkdir(path)
except OSError as error:
    print(error)

try:
    os.chdir(path)
except:
    print("chdir() error")

selection = []
for i in range(len(locs)):
    selection.append(random.choices(locs[i], k=1)[0])

for i in range(len(selection)):
    with open((str(currentTime)+"_"+selection[i]+".jpg"), "wb") as photo:
        photo.write(urllib.request.urlopen("https://www.hko.gov.hk/wxinfo/aws/hko_mica/"+selection[i].lower()+"/latest_HD_"+selection[i]+".jpg").read())
