import json
import os

def avg_from_dict(data, substring):
    avg = 0
    len = 0
    for key in data:
        if substring in key:
            if data[key] != "":
                try:
                    avg += float(data[key])
                    len += 1
                except ValueError:
                    print(f"Error in {key}: {data[key]} is invalid")
    avg = avg/len
    return avg

def max_from_dict(data, substring):
    max = -99999.9
    for key in data:
        if substring in key:
            if data[key] != "":
                try:
                    if max < float(data[key]):
                        max = float(data[key])
                except ValueError:
                    print(f"Error in {key}: {data[key]} is invalid")
    return max

def get_from_date(date: str):
    data = json.load(open(f"{date}/ryes.json", "r"))
    #avg = avg_from_dict(data, "MaxTemp")
    #print(f"Average max temp: {avg:#.2f}")
    #max = max_from_dict(data, "MaxTemp")
    #print(f"Max max temp: {max:#.2f}")
    #avg = avg_from_dict(data, "MinTemp")
    #print(f"Average min temp: {avg:#.2f}")
    print(f"Max temp: {data['HKOReadingsMaxTemp']}")
    print(f"Min temp: {data['HKOReadingsMinTemp']}")
    print(f"Max RH:   {data['HKOReadingsMaxRH']}")
    print(f"Min RH:   {data['HKOReadingsMinRH']}")
    print(f"Rainfall: {data['HKOReadingsRainfall']}")

if __name__ == "__main__":
    dir = sorted(os.listdir("./"))
    for item in dir:
        if os.path.isdir(item):
            if os.path.exists(f"{item}/ryes.json"):
                print(f"{item}")
                get_from_date(item)
